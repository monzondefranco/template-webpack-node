import express from 'express';
import morgan from 'morgan';
import router from './router'
import dotenv from 'dotenv';

//sirve para leer .env
dotenv.config();

//creamos la app y habilitamos morgan, que es una vitacora de los llamados a la API
const app = express();
app.use( morgan('dev') )

router( app );

const port = process.env.NODE_PORT

app.listen( port, ()=> {
    console.log('running on port ' + port)
})