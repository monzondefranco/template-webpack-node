import tasksRoutes from './entities/tasks/routes'


function router ( app ) {
    app.use( '/tasks', tasksRoutes);
    app.use( '/users', (req, res) => {
        res.json( { users: [] } )
    })
}

export default router